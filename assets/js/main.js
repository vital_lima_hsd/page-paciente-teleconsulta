$(function() {
	const app = {
		init(){
			this.events();
		},
		events () {
			$('.button-mobile').on('click', function(event) {
				event.preventDefault();
				app.methods.openMenu();
			});
			$('.btn-close, .menu-item a').on('click', function(event) {
				app.methods.closeMenu();
			});
			$('.btn-back').on('click', function(event) {
				history.go(-1);
			});
		},
		methods: {
			openMenu(){
				$('.main-menu').addClass('show-mobile');
			},
			closeMenu(){
				$('.main-menu').removeClass('show-mobile');
			}
		}
	}

	app.init();
});