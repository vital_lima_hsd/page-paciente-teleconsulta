export default function ({ store, $axios, redirect, route }, inject) {
	// Create a custom axios instance
	const api = $axios.create({
		withCredentials: true,
		headers: {
			common: {
				Accept: 'application/json'
			}
		}
	})

	// const url = 'https://...'

	// axios.post(url, {
	// 	headers: {
	// 		'Authorization': `Basic ${token}`
	// 	}
	// })

	// Set baseURL to something different
	// axios.defaults.baseURL = 'http://192.168.3.149:57772/PanoramaClinico/';
	// axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
	// axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

	// axios.defaults.withCredentials = true;

	api.setHeader('Content-Type', 'application/xml;charset=utf-8')
	api.setHeader('Access-Control-Allow-Origin', '*')
	// api.setHeader('Access-Control-Allow-Credentials', true)
	// api.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
	// api.setHeader('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept')
	// api.setBaseURL('http://gtwesbhomol01.hospitalsaodomingos.com.br:3148/api')
	
	// const username = 'hsdservice'
	// const password = 'j!6#gs1lWVyn'

	// const token = Buffer.from(`${username}:${password}`, 'utf8').toString('base64')
	// api.setToken(token, 'Bearer');

	// Inject to context as $api
	inject('api', api)
}
