
export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    script: [
      { src: 'https://unpkg.com/blip-chat-widget@1.6.*' }
    ],
    title: 'Minha teleconsulta - #EMCASA',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { property: 'og:locale', content: 'pt_BR'},
      // { 'http-equiv': 'Content-Security-Policy', content: 'upgrade-insecure-requests' },
      { property: 'og:image', content: 'https://www.hospitalsaodomingos.com.br/teleconsulta/sou.jpg'},
      { property: 'og:image:type', content: 'image/jpeg'},
      { property: 'og:image:width', content: '1536'},
      { property: 'og:image:height', content: '1536'},
      { property: 'og:title', content: '#EMCASA'},
      { property: 'og:description', content: 'Agora você pode ter os melhores dentro da sua casa'},
      { property: 'og:url', content: 'https://www.hospitalsaodomingos.com.br/teleconsulta/'},
      { property: 'og:site_name', content: '#EMCASA'},
      { hid: 'description', name: 'description', content: 'Agora você pode ter os melhores dentro da sua casa' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/teleconsulta/favicon.ico' }
    ]
  },
  router: {
    base: '/teleconsulta/'
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/axios',
    // '~/plugins/blip'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
